let filterBelowRange = (containers) =>{
    return containers.filter(e => e.minT>e.temperature)
}

let filterAboveRange = (containers) =>{
    return containers.filter(e => e.maxT<e.temperature)
}

export {
  filterBelowRange,
  filterAboveRange,
}
