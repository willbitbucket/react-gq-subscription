import React from 'react';
import ContainerDetail from '../../components/ContainerDetail';

import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';


let r = {
  id: '1',
  type:'Pilsner',
  minT: 4,
  maxT: 6,
  temperature: 3
}

describe('ContainerDetail unit test', function() {

  it('should exist', function() {
      const data = {loading:false, error: null, container:r, subscribeToMore: () => {} }

      let c = TestUtils.renderIntoDocument(<ContainerDetail data={data}/>);
      expect(TestUtils.isCompositeComponent(c)).toBeTruthy();
    });

  it('should build the container passed as prop', function() {
    const data = {loading:false, error: null, container:r, subscribeToMore: () => {}}

    let c = TestUtils.renderIntoDocument(<ContainerDetail data={data}/>);

    let buttons = TestUtils.scryRenderedDOMComponentsWithClass(c, 'position');
    expect(buttons.length).toEqual(3);
  });


});
