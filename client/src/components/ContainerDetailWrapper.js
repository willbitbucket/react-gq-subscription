import React from 'react';
import {
    gql,
    graphql,
} from 'react-apollo';

import ContainerDetail from './ContainerDetail';

export const query = gql`
query ContainerQuery($id : ID!) {
  container(id: $id) {
    id
    type
    temperature
    maxT
    minT
  }
}
`;


export default (graphql(query, {
  options: (props) => ({
    variables: { id: props.match.params.id }
  }),
})(ContainerDetail));
