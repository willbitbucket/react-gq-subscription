import React, { Component } from 'react';

import {
    gql,
} from 'react-apollo';

import { ToastContainer, toast } from 'react-toastify';

import {filterAboveRange, filterBelowRange} from '../service/container';

class Containers extends Component {
  componentWillMount() {
    this.props.data.subscribeToMore({
      document: update,
      variables: {

      },
      updateQuery: (state, {subscriptionData}) => {
          if(subscriptionData.data.temperatureUpdated){
            filterAboveRange(subscriptionData.data.temperatureUpdated).forEach(e => toast.error(`${e.id} ${e.type} is ${e.temperature} °C, above ${e.maxT} °C`,
             { position: toast.POSITION.TOP_RIGHT }))
            filterBelowRange(subscriptionData.data.temperatureUpdated)
            .forEach(e => toast.warn(`${e.id} ${e.type} is ${e.temperature} °C, below ${e.minT} °C`,{ position: toast.POSITION.TOP_RIGHT }))
          return {containers: subscriptionData.data.temperatureUpdated}
        }else return {containers: state}

      }
    })
  }

  render() {
    const { data: {loading, error, containers } } = this.props;

    if (loading) {
      return (<div  className="channelName">Loading...</div>);
    }
    if (error) {
      return (<div  className="channelName">{error.message}</div>);
    }
    if(containers === null || containers === undefined){
      return (<div  className="channelName">No containers yet...</div>);
    }

    return (
      <div>
        <ToastContainer autoClose={4000}/>
        <div className="messagesList">
        {
          containers.map( (r, i) =>
          {
          return (
            <div className="message" key={i}>
                <a href={ `container/${r.id}`}>
                 Container {r.id} ({r.type}), requires:[{r.minT}, {r.maxT}], measured:{r.temperature} °C
                </a>
            </div>
           )}

        )}
        </div>
      </div>);
  }
}


const update = gql`
  subscription temperatureUpdated{
    temperatureUpdated {
      id
      type
      temperature
      maxT
      minT
       }
  }
`

export default Containers;
