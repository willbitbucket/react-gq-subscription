import React, { Component } from 'react';
import Containers from './Containers';

import {
    Link
} from 'react-router-dom'


import {
    gql,
    graphql,
} from 'react-apollo';

const query = gql`
  query ContainersQuery {
    containers {
      id
      type
      temperature
      maxT
      minT
    }
  }
`;

export default (graphql(query, {
  options: (props) => ({
      fetchPolicy: 'network-only'
  }),
})(Containers));
