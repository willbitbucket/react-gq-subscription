import { gql } from 'react-apollo';
import React, { Component } from 'react';
class ContainerDetail extends Component {
  componentWillMount() {
    this.props.data.subscribeToMore({
      document: update,
      variables: {

      },
      updateQuery: (state, {subscriptionData}) => {
          if(subscriptionData.data.temperatureUpdated){
            return {container: subscriptionData.data.temperatureUpdated.find(container => container.id === this.props.match.params.id)}
        }else return {container: state}
      }
    })
  }

  render() {
    const { data: {loading, error, container } } = this.props;

  if (loading) {
    return (<div  className="channelName">Loading...</div>);
  }
  if (error) {
    return (<div  className="channelName">{error.message}</div>);
  }
  if(container === null || container === undefined){
    return (<div  className="channelName">No container yet...</div>);
  }
  return (
    <div>
      <div className="channelName">
        <div className="title">Container {container.id} ({container.type}) </div>
      <p></p>
          <div>
              <label className="position">temperature: {container.temperature}°C</label>&nbsp;
          </div>
          <p></p>
          <div>
              <label className="position">min: {container.minT}°C</label>&nbsp;
              <button className="bet" type="submit">+</button>&nbsp;
              <button className="bet" type="submit">-</button>
          </div>
          <p></p>
          <div>
              <label className="position">max: {container.maxT}°C</label>&nbsp;
              <button className="bet" type="submit">+</button>&nbsp;
              <button className="bet" type="submit">-</button>
          </div>
      </div>
    </div>);
  }
}

const update = gql`
  subscription temperatureUpdated{
    temperatureUpdated {
      id
      type
      temperature
      maxT
      minT
       }
  }
`

export default ContainerDetail;
