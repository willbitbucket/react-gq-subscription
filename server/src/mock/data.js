/*
- Beer 1 (Pilsner) - 4°C - 6°C
- Beer 2 (IPA) - 5°C - 6°C
- Beer 3 (Lager) - 4°C - 7°C

- Beer 4 (Stout) - 6°C - 8°C
- Beer 5 (Wheat beer) - 3°C - 5°C
- Beer 6 (Pale Ale) - 4°C - 6°C
*/
let containers = [
  {
    id: '1',
    type:'Pilsner',
    minT: 4,
    maxT: 6,
    temperature: 3
  },
  {
    id: '2',
    type:'IPA',
    minT: 5,
    maxT: 6,
    temperature: 5
  },
  {
    id: '3',
    type:'Lager',
    minT: 4,
    maxT: 7,
    temperature: 5
  },
  {
    id: '4',
    type:'Stout',
    minT: 6,
    maxT: 8,
    temperature: 7
  },
  {
    id: '5',
    type:'Wheat beer',
    minT: 3,
    maxT: 5,
    temperature: 4
  },
  {
    id: '6',
    type:'Pale Ale',
    minT: 4,
    maxT: 6,
    temperature: 5
  }]

let mutateContainersData = (containers) =>{
  let n = Math.round(Math.random()*containers.length)
  for(let i =0; i< n; i++){
    let index = Math.round(Math.random()*(containers.length-1))
    containers[index].temperature = Math.round((containers[index].temperature + (Math.random()-0.5)/5) * 100) / 100
  }
  return containers;
}




export function containers(){ return containers }
export {
  mutateContainersData,

}
