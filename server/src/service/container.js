import * as containerDao from '../dao/container'
let findContainerById = (id) => {
  return containerDao.get(id)
}
let findContainers = () => {
  return containerDao.getAll()
}
export {
  findContainerById,
  findContainers
}
