import * as service from '../../service/container';
import { expect } from "chai";
import sinon  from 'sinon';

let c1 = {
  id: '1',
  type:'Pilsner',
  minT: 4,
  maxT: 6,
  temperature: 3
}

describe('container service unit test', function() {
    describe('get by id', function () {
        it("returns mocked container by id", function (done) {
            let stub = sinon.stub(require('../../dao/container'), 'get');
            stub.returns(c1);
            expect(JSON.stringify(service.findContainerById('1'))).to.equal(JSON.stringify(c1));
            stub.restore();
            done()
        });
    })

    describe('get all containers', function () {
        it("returns mocked containers", function (done) {
            let stub = sinon.stub(require('../../dao/container'), 'getAll');
            stub.returns([c1]);
            expect(JSON.stringify(service.findContainers())).to.equal(JSON.stringify([c1]));
            stub.restore();
            done()
        });
    })

    describe('get non-existent container', function () {
      it("should return null", function (done) {
          let stub = sinon.stub(require('../../dao/container'), 'get');
          stub.returns(null);
          expect(service.findContainerById('1')).to.equal(null);
          stub.restore();
          done()
      });
  })



    afterEach(()=>{
        //require('request-promise').post.restore()
    })

})
