import * as dao from '../../dao/container';
import { expect } from "chai";
import sinon  from 'sinon';


let c1 = {
  id: '1',
  type:'Pilsner',
  minT: 4,
  maxT: 6,
  temperature: 3
}

describe('container dao unit test', function() {

    describe('get all', function () {
        it("returns mocked container", function (done) {
            let stub = sinon.stub(require('../../mock/data'), 'containers');
            stub.returns([ c1 ]);
            expect(JSON.stringify(dao.getAll())).to.equal(JSON.stringify([c1]));
            stub.restore();
            done()
        });
    })
    describe('get by id', function () {
        it("returns mocked container id", function (done) {
            let stub = sinon.stub(require('../../mock/data'), 'containers');
            stub.returns([ c1 ]);
            expect(JSON.stringify(dao.get('1'))).to.equal(JSON.stringify(c1));
            stub.restore();
            done();
        });
    })



    afterEach(()=>{
        //require('request-promise').post.restore()
    })

})
