import {
  makeExecutableSchema,
  addMockFunctionsToSchema,
} from 'graphql-tools';

import { resolvers } from './resolvers';

const typeDefs = `
type Container {
  id: ID!                # "!" denotes a required field
  type: String
  maxT: Float
  minT: Float
  temperature: Float
}

# This type specifies the entry points into our API
type Query {
  container(id: ID!): Container
  containers: [Container]
}

# The mutation root type, used to define all mutations
# type Mutation {
# }

# The subscription root type, specifying what we can subscribe to
type Subscription {
  temperatureUpdated: [Container]
}
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });
export { schema };
