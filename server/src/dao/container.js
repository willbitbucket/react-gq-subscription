import { races, mutateRacesData, containers, mutateContainersData } from '../mock/data';


let get = (id) => {
  return containers().find(container => container.id === id)
}
let getAll = () => {
  return containers()
}

export {
  get,
  getAll
}
