import { PubSub } from 'graphql-subscriptions';
import { withFilter } from 'graphql-subscriptions';

import { channels, races, mutateRacesData, mutateContainersData } from './mock/data';

import * as containerService from './service/container'

let nextId = 3;
let nextMessageId = 5;

const pubsub = new PubSub();

setInterval(()=>{
    pubsub.publish('temperatureUpdated', { temperatureUpdated:mutateContainersData(containerService.findContainers()) })
},2000)

export const resolvers = {
  Query: {
    container: (root, { id }) => {
        return containerService.findContainerById(id);
    },
    containers: () => {
        return containerService.findContainers();
    },
  },

  Subscription: {
   temperatureUpdated: {
      subscribe: withFilter(() => pubsub.asyncIterator('temperatureUpdated'), (payload, variables) => {
            return true;
      }),
    }
  },
};
