# Temperature Notification:
## Assumptions:
1. sensor and the mobile (I don't assume a server-side API endpoint is practical for this problem, instead API endpoint within the same local network should be more practical) can be connected via internet (wlan or local internet), or blue tooth, however how to realize this is out of the scope of this solution. This could be realized in next iteration, by nodejs mobile container:
https://github.com/jxcore/jxcore/blob/master/doc/INSTALLATION.md
https://code.janeasystems.com/nodejs-mobile#learn-more  
2. sensors will be able (i) to automatically push data to mobile at certain intervals or (ii) polled by mobile on ad-hoc, and it is assumed that (i) will be done in this solution.
3. For simplicity, the connection between mobile and sensor via http is implemented in this solution.

To sum up, this solution includes a react UI with apollo-client connecting to a graphql API server backed by nodejs, via graphql query and subscription. The mutation API to change container beer temperature range, as well as manage containers including number, bear type are not implemented yet in this solution, which may be done in next iteration.

### This repo contains an API server and a web server.

```
cd server
npm install
npm test
```

```
cd ../client
npm install
npm start
npm test
```

1. API document: http://localhost:4000/graphiql
2. temperature notification UI: http://localhost:3000/
